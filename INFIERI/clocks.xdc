# Specify clock frequency input to FPGA from board.
create_clock -period 5.000 -name SYSCLK_P -waveform {0.000 2.500} [get_ports SYSCLK_P]

# Specify derived clock (in top.vhd) and frequency ratio to CLK.
#create_generated_clock -name CLKslow -source [get_pins #makeClock/MMCME/CLKOUT1] -divide_by 8 [get_pins CLKslow_reg/Q]
