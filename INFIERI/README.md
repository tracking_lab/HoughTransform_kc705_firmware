Hough Tranform track reconstruction algorithm theory
=====================================================

Detailed documentation on the lab & instructions for doing it are in **https://gitlab.cern.ch/tracking_lab/kc705_firmware/-/blob/master/INFIERI/Tracker_lab.pdf**, (which was compiled from the latex document Overleaf in the (read only) link
https://www.overleaf.com/read/swmrxqsgdxxv ). The lab is advertised in slide 30 of https://indico.cern.ch/event/850479/attachments/1916961/3877341/INFIERI_v10.pdf .

Input Tracker hit data
=======================

This is can be found in INFIERI/data/ , and consists of simulated data produced by the Jupyter notbook INFIERI/JupyterNotebook/TrackerLab.ipynb. This invents a few random particles and simulates the hits produces when they cross the layers of the Tracker. The Tracker consists of 11 barrel cylindrical layers of radii 0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2 metres.

The simulated data in INFIERI/data/ consists of:
a) qOverPt_0.0/ (3 infinite Pt particles)
b) qOverPt_0.6/ (3 low Pt particles)
c) qOverPt_0.0to0.65/ (30 particles in a range of Pt). 
d) qOverPt_0.0to0.65_big/ (300 particles in a range of Pt). 

Examine these in order, making sure you understand how the Hough transform is responding to them. (Edit src_trk/tb.cc & src_hits/readHits.h to switch between them.). Try changing the parameters controlling the Hough transform tracking in src_trk/trkReco.h (minHits, nBinsR, nBinsPhi0, shiftInvR & shiftPhi0).

(Repo owners can edit/run the Jupyter notebook using by copying it to 
CERN's Swan system. e.g. https://swan004.cern.ch/user/tomalin/projects/INFIERI_school_2021 )

Optimizing & understanding the HLS Hough transform track reconstruction
========================================================================

In directory INFIERI/, you can compile HLS code, which is in HLS/src_trk/, for reconstructing tracks with a Hough transform with:

_vivado_hls -f vivado_hls_trk.tcl_

Initially, edit vivado_hls_trk.tcl, so it only runs:

    "csim" (compilation of code with C++ compiler). 

This is fast, and allows debug printout to be included in your HLS algorithm. The test-bench tb.cc injects test-data (Tracker hits from INFIERI/data/) into the algorithm and print out the achieved tracking efficiency & helix parameter resolution. 

This printout is written to INFIERI/vivado_hls.log.

Try running the (slower) additional compilation steps, 

    "synth" (convert code to VHDL/Verilog languages), 
    "cosim" run simulation using the test-bench again, but now with the VHDL/Verilog code,
    "export" make the HLS IP Core.

All compiler output goes to WorkHLS_trk/. Interesting are:

* _WorkHLS_trk/solution1/syn/report/trkReco_csynth.rpt_ (the report stating the latency & FPGA resource usage of the design, if it can run at the requested FPGA frequency, and if it can pipeline with the requested input data interval).
* _WorkHLS_trk/solution1/impl/vhdl_ (the VHDL written by the compiler)
* _WorkHLS_trk/solution1/impl/ip/xilinx_com_hls_trkReco_1_0.zip_ (the HLS IP core)


*USING THE VIVADO HLS GUI:*

Open the GUI with this. Look at the reports there. Look at the signal wave display. And use it to get more detailed info if your design failed to meet timing or used too many FPGA resources.

_vivado_hls -p WorkHLS_trk/_

QUESTIONS: 
1) Do the reported BRAM (=memory) & DSP (=multiplier) use in the FPGA make sense to you, given the array sizes booked in the HLS code & the number of multiplications done there? 
2) Does the reported latency make sense? How is it defined? In output file vivado_hls.log, is the line "Track reco finished after function call 434 etc.". How does this relate to the latency?
3) The reported pipeine interval = 2, so the code can only accept a new input hit every 2 clock cycles -- is it possible to modify the code to reduce it to 1?
4) vivado_hls_trk.tcl asks the compiler to produce an HLS IP core compatible with running at 100MHz on the FPGA. What happens if you request 500MHz? Does it work?

Compiling entire project
=========================

This is slow, so only do it once or twice:

In directory INFIERI/:

1) Compile HLS code (src_trk/) for reconstructing tracks:

_vivado_hls -f vivado_hls_trk.tcl_

2) Compile HLS code (std_hits/) for reading input hits from hits.txt file.

_vivado_hls -f vivado_hls_hits.tcl_

3) Compile HLS code (src_uart/) for transmitting data about reconstructed tracks over USB/UART line to Linux PC.

_vivado_hls -f vivado_hls_uart.tcl_

4) Create a Vivado project and run Vivado simulation on the VHDL code (hdl/) that instantiates (1) & (2) & (3. The top-level code is hdl/top.vhd and hdl/tb.vhd is the corresponding test-bench.

_vivado -mode batch -source vivado_sim.tcl_

5) Open the compiled project with the Vivado GUI. Run the Vivado simulation there, checking in the wave displays if tracks are being reconstructed etc.

_vivado WorkVHDL/WorkVHDL.xpr &_

6) Synthesize & implement the Vivado project, producing 
WorkVHDL/WorkVHDL.runs/impl_1/top.bit .

_vivado -mode batch -source vivado_compile.tcl_

7) Open a new Linux window, to capture the data output (reconstructed tracks) from the FPGA. See the "Notes on UART@ section below. If interested, take the time to understand how UART data transmission over a serial link from FPGA to computer works.

8) Load .bit file onto board, and optionally press a button to launch it (though by default it starts without this). 

_vivado -mode batch -source vivado_loadbitfile.tcl_

You should see the number of reconstructed tracks appear on the LCD display of the board, and the digitized word corresponding to each reconstructed track appear in your new Linux window.

Further Exercises
==================

1) Invent improvements to the src_trk/ code to improve tracking performance, especially with many particles per event. e.g. Optimise the choice of parameters in trkReco.h. Or modify the HT to work with hit r & track phi measured with at a radius of ~60cm instead of 0cm from the beam-line. Or run independent track-finding in in sectors in tanL.
2) Invent improvements to reduce the FPGA resource use or latency. 
3) Figure out how design could process more than 1 event.

Notes on UART serial transmission of data FPGA to the Linux PC
================================================================

This is done by UART over the USB link. It uses a single transmit line. To transmit a single hex number, the Ascii code corresponding to that number is obtained. This is 8 bits. We must transmit a '0', followed by these 8 bits, followed by a '1'. Each bit must be held for a time given by the period corresponding to the "baud rate clock frequency", taken to be 115.2kHz (the fastest UART can support). More info in https://www.circuitbasics.com/basics-uart-communication .

To enable data transmission over USB/UART:

a) Check if you are a member of the "dialout" group, (allowed to send data along USB), and if not add yourself:

```
groups $USER
sudo adduser $USER dialout
```

b) With board switched on and connected via USB to PC, check you can see the USB port

```
ls -l /dev/ttyUSB* (P.S. This also shows that these ports are owned by the "dialout" group)
lsusb
usb_devices
```

c) Set the baud rate and protocol of the UART/USB link, and check it worked:

```
stty -F /dev/ttyUSB0 115200 cs8 -cstopb -parenb
stty -F /dev/ttyUSB0 
```

d) To print data sent from FPGA to both screen & to file:

```
stdbuf -o0 cat /dev/ttyUSB0 | tee myFile.txt
```

N.B. If the computer is rebooted, you must repeat (c).

How to play with Vivado & FPGA programming in your own time on your own computer
=================================================================================

1) Install the free version of Vivado by downloading the "Self extracting web installer" for either Linux or Windows from https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/archive.html . (Vivado is not available for MAC).
2) If you want your own FPGA board to play with, the Basys3 is cheap and good https://digilent.com/reference/programmable-logic/basys-3/start?redirect=1 , and comes with example code.
3) The free version of Vivado can only compile code for small FPGAs. So in the .tcl files for this real-time tracking project, you'd need to change the specified FPGA. e.g. To the XC7A35T-1CPG236C , which is the small FPGA on the Basys3. 

N.B. After 2020.1, Vivado HLS was replaced with Vitis HLS, which is very similar, but different. So the HLS tracking code of this project would not work out of the box with it. We suggest you start with 2020.1
