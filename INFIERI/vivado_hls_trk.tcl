#---------------------------------------------------------------
# Compile HLS IP core for reconstructing tracks from hits
# and writing them to BRAM memory.
#---------------------------------------------------------------

delete_project WorkHLS_trk
open_project WorkHLS_trk

set CFLAGS {-std=c++11}

set_top trkReco
add_files     src_common/trkMemorySpec.h  -cflags "$CFLAGS"
add_files     src_trk/trkReco.h           -cflags "$CFLAGS"
add_files     src_trk/trkReco.cc          -cflags "$CFLAGS"
add_files -tb src_trk/tb.cc               -cflags "$CFLAGS"

set solution solution1
delete_solution $solution
open_solution $solution

# KC705 FPGA
set_part {xc7k325tffg900-2} -tool vivado

create_clock -period 100MHz -name default

# Allow HLS to use longer (easier to understand) variable names in reports.
config_compile -name_max_length 100
# Prevent HLS relaxing pipeline interval to meet timing
config_schedule -relax_ii_for_timing=0

csim_design -clean -compiler gcc -mflags "-j8"
csynth_design
cosim_design -trace_level all -rtl vhdl -O
export_design -rtl vhdl -format ip_catalog

exit
