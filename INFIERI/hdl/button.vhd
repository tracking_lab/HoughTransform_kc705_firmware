library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

---------------------------------------------------------
-- Detect press of central button.
-- It must be held down for several clock cycles,
-- to avoid jitter.
---------------------------------------------------------

-- Provides arithmetic for signed & unsigned values
use IEEE.NUMERIC_STD.ALL;

entity button is
    Generic ( USE_BUTTON : boolean);
    Port ( CLK     : in  std_logic;
	   BTN     : in std_logic;    --! Button
           PRESSED : out std_logic);  --! Button registered as pressed.
end button;

architecture Behavioral of button is

  signal PRESS : std_logic := '0';
               
begin

  btn_detect : process(CLK)
    constant maxCnt : INTEGER := 31; -- Button must be held down this long.
    variable count : INTEGER RANGE 0 to maxCnt := 0;
  begin
    if USE_BUTTON then
      if rising_edge(CLK) then
        if BTN = '1' then
          if (count < maxCnt) then
            count := count + 1;
          else
            PRESS <= '1';
          end if;
        else
          count := 0;
        end if;
      end if;
    end if;
  end process;

  PRESSED <= PRESS;

end Behavioral;
