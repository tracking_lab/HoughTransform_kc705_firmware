-------------------------------------------------------------------------------
-- Title      : Synthesizable demo for design "lcd16x2_ctrl"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : lcd16x2_ctrl_tb.vhd
-- Author     :   <stachelsau@T420>
-- Company    : 
-- Created    : 2012-07-28
-- Last update: 2012-07-29
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: This demo writes writes a "hello world" to the display and
-- interchanges both lines periodically.
-------------------------------------------------------------------------------
-- Copyright (c) 2012 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2012-07-28  1.0      stachelsau      Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-------------------------------------------------------------------------------

entity lcd16x2_ctrl_int is
  port (
    clock    : in  std_logic;
    LCD_E  : out std_logic;
    LCD_RS : out std_logic;
    LCD_RW : out std_logic;
    LCD_DB4: out std_logic;
    LCD_DB5: out std_logic;
    LCD_DB6: out std_logic;
    LCD_DB7: out std_logic;
    NumTracks:  in std_logic_vector(7 downto 0));
    --lcd_db : out std_logic_vector(7 downto 4));

end entity lcd16x2_ctrl_int;

-------------------------------------------------------------------------------

architecture behavior of lcd16x2_ctrl_int is

  -- 
  signal timer : natural range 0 to 100000000 := 0;
  signal switch_lines : std_logic := '0';
  signal line1 : std_logic_vector(127 downto 0);
  signal line2 : std_logic_vector(127 downto 0);
  signal lcd_db:  std_logic_vector(7 downto 4);

  signal pos0, pos1, pos2 : natural range 0 to 9;
  signal numtot : natural range 0 to 999;
  -- component generics
  constant CLK_PERIOD_NS : positive := 64;  -- 156.25 Mhz

  -- component ports
  signal rst          : std_logic;
  signal line1_buffer : std_logic_vector(127 downto 0);
  signal line2_buffer : std_logic_vector(127 downto 0);

begin  -- architecture behavior

  LCD_DB4 <= lcd_db(4);
  LCD_DB5 <= lcd_db(5);
  LCD_DB6 <= lcd_db(6);
  LCD_DB7 <= lcd_db(7);


  -- component instantiation
  DUT : entity work.lcd16x2_ctrl
    generic map (
      CLK_PERIOD_NS => CLK_PERIOD_NS)
    port map (
      clk          => clock,
      rst          => rst,
      lcd_e        => LCD_E,
      lcd_rs       => LCD_RS,
      lcd_rw       => LCD_RW,
      lcd_db       => lcd_db,
      line1_buffer => line1_buffer,
      line2_buffer => line2_buffer);

  rst <= '0';

  -- see the display's datasheet for the character map
  line1(127 downto 120) <= X"20"; 
  line1(119 downto 112) <= X"54";  -- T
  line1(111 downto 104) <= X"72";  -- r
  line1(103 downto 96)  <= X"61";  -- a
  line1(95 downto 88)   <= X"63";  -- c
  line1(87 downto 80)   <= X"6b";  -- k
  line1(79 downto 72)   <= X"73";  -- s
  line1(71 downto 64)   <= X"27";  -- '
  line1(63 downto 56)   <= X"20";  -- 
  line1(55 downto 48)   <= X"23";  -- #
  line1(47 downto 40)   <= X"3d";  -- =
  numtot <= TO_INTEGER(unsigned(NumTracks));
  pos0 <= numtot mod 10;
  pos1 <= (numtot / 10) mod 10;
  pos2 <= (numtot / 100) mod 10;
  line1(39 downto 32)   <= std_logic_vector(to_unsigned(pos2,8)+x"30");
  line1(31 downto 24)   <= std_logic_vector(to_unsigned(pos1,8)+x"30");
  line1(23 downto 16)   <= std_logic_vector(to_unsigned(pos0,8)+x"30");
  line1(15 downto 8)    <= X"20";
  line1(7 downto 0)     <= X"20";

  line2(127 downto 120) <= X"20";
  line2(119 downto 112) <= X"20";
  line2(111 downto 104) <= X"54";  -- T
  line2(103 downto 96)  <= X"72";  -- r
  line2(95 downto 88)   <= X"61";  -- a
  line2(87 downto 80)   <= X"63";  -- c
  line2(79 downto 72)   <= X"6b";  -- k
  line2(71 downto 64)   <= X"65";  -- e
  line2(63 downto 56)   <= X"72";  -- r
  line2(55 downto 48)   <= X"20";  
  line2(47 downto 40)   <= X"4c";  -- L
  line2(39 downto 32)   <= X"61";  -- a
  line2(31 downto 24)   <= X"62";  -- b
  line2(23 downto 16)   <= X"20";
  line2(15 downto 8)    <= X"20";
  line2(7 downto 0)     <= X"20";

  --line1_buffer <= line2 when switch_lines = '1' else line1;
  --line2_buffer <= line1 when switch_lines = '1' else line2;

  line1_buffer <= line1;
  line2_buffer <= line2;  

  -- switch lines every second
  process(clock)
  begin
    if rising_edge(clock) then
      if timer = 0 then
        timer <= 100000000;
        switch_lines <= not switch_lines;
      else
        timer <= timer - 1;
      end if;
    end if;
      
  end process;
end architecture behavior;


