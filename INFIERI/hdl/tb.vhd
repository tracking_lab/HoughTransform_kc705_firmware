----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.05.2021 13:27:18
-- Design Name: 
-- Module Name: tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Provides arithmetic for signed & unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity tb is
--  Port ( );
end tb;

architecture Behavioral of tb is
constant CLK_PERIOD : time := 5 ns; --! 200 MHz clock from KC705 board
signal CLK_P  : std_logic := '0'; --! Differential clock
signal CLK_N  : std_logic := '0'; --! Differential clock
signal BTN    : std_logic := '0'; --! Push button
signal UART_TX : std_logic := '0'; --! UART/USB transmit signal
signal LED    : std_logic_vector (4 downto 0) := (others => '0'); --! LED lights 
signal LCD    : std_logic_vector (6 downto 0) := (others => '0'); --! LCD display

begin

CLK_P <= not CLK_P after CLK_PERIOD/2;
CLK_N <= not CLK_P;

-- Button on board used to launch track reconstruction.
BTN <= not BTN after 1000ns;

mut : entity work.top
  port map (
     SYSCLK_P => CLK_P,
     SYSCLK_N => CLK_N,
     GPIO_SW_C => BTN,
     USB_RX => UART_TX, -- KC705 confusingly names UART transmit "RX"
     GPIO_LED_0_LS  => LED(0),
     GPIO_LED_1_LS  => LED(1),
     GPIO_LED_2_LS  => LED(2),
     GPIO_LED_3_LS  => LED(3),
     GPIO_LED_4_LS  => LED(4),
     LCD_E_LS   => LCD(0),
     LCD_RS_LS  => LCD(1),
     LCD_RW_LS  => LCD(2),
     LCD_DB4_LS => LCD(3),
     LCD_DB5_LS => LCD(4),
     LCD_DB6_LS => LCD(5),
     LCD_DB7_LS => LCD(6)
  );

end Behavioral;
