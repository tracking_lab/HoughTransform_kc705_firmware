library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Provides arithmetic for signed & unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--================================================================
-- Compile top-level VHDL that instantiates all the HLS IP cores
-- Runs track reconstruction, stores found tracks in a BRAM
-- and transmits contents of BRAM over USB/UART to Linux.
--================================================================

entity top is
    Port ( SYSCLK_P  : in  std_logic;     --! 200 MHz diff. clock for KC705
           SYSCLK_N  : in  std_logic;
	   GPIO_SW_C : in  std_logic;     --! Push button
	   USB_RX    : out std_logic;     --! USB/UART transmit signal (confusingly called RX)
           GPIO_LED_0_LS : out std_logic; --! LED lights
           GPIO_LED_1_LS : out std_logic;
           GPIO_LED_2_LS : out std_logic;
           GPIO_LED_3_LS : out std_logic;
           GPIO_LED_4_LS : out std_logic;
           LCD_E_LS  : out std_logic;     --! LCD display
           LCD_RS_LS : out std_logic;
           LCD_RW_LS : out std_logic;
           LCD_DB4_LS: out std_logic;
           LCD_DB5_LS: out std_logic;
           LCD_DB6_LS: out std_logic;
           LCD_DB7_LS: out std_logic);
end top;

architecture Behavioral of top is

-- Start program by pressing button on board (or automatically when program loaded into FPGA)?
constant USE_BUTTON : boolean := FALSE;

-- Size of RAM for storing tracks
constant WIDTH_BITS : natural := 32;
constant DEPTH_BITS : natural := 8;
-- Width in bits of a hit;
constant HIT_BITS : natural := 64;

-- FPGA clock (single line) and slow clock (scaled down by factor 16)
signal CLK, CLKslow : std_logic := '0';

-- Button press on board launches track reco (if USE_BUTTON = TRUE).
signal BTN, START_BTN : std_logic := '0';

-- Control signals for HLS IP Cores.
signal RESET, RESET_REG1, RESET_REG2 : std_logic := '1';
signal START_HIT, RESET_HIT, READY_HIT : std_logic := '0';
signal START_TRK, READY_TRK, DONE_TRK, FINISHED_TRK, FINISHED_TRK_LATCH, FINISHED_TRK_REG1, FINISHED_TRK_REG2, DONE_TRK_READ_HITS : std_logic := '0';
signal FINISHED_COUNT : std_logic := '0';
signal READY_UART,  DONE_UART, FINISHED_UART : std_logic := '0';

-- Signals of hit FIFO.
signal READ_FIFO, LAST_HIT : std_logic := '0';
signal HIT : std_logic_vector(HIT_BITS - 1 downto 0) := (others => '0'); 

-- Signal sent along UART/USB link to Linux.
signal UART_TX : std_logic := '1';
signal UART_TX_LATCH : std_logic := '0'; -- Says if there has been any UART/USB activity.

-- Data access to BRAM memory used to store reconstructed tracks.
signal READ_ENABLE  : std_logic := '0';
signal READ_ADDR    : std_logic_vector(DEPTH_BITS - 1 downto 0) := (others => '0');
signal READ_DATA    : std_logic_vector(WIDTH_BITS - 1 downto 0) := (others => '0');
signal WRITE_ENABLE : std_logic := '0';
signal WRITE_ADDR   : std_logic_vector(DEPTH_BITS - 1 downto 0) := (others => '0');
signal WRITE_DATA   : std_logic_vector(WIDTH_BITS - 1 downto 0) := (others => '0');

signal NTRACKS, NTRACKS_REG1, NTRACKS_REG2 : std_logic_vector(DEPTH_BITS - 1 downto 0) := (others => '0');

begin

-- Map UART transmission signal to more sensible name.
USB_RX <= UART_TX;

-- Map button to simpler name.
BTN <= GPIO_SW_C;

-- Connect LED lights to chosen signals.
GPIO_LED_0_LS <= START_HIT;
GPIO_LED_1_LS <= FINISHED_TRK;
GPIO_LED_2_LS <= FINISHED_UART;
GPIO_LED_3_LS <= UART_TX;
GPIO_LED_4_LS <= UART_TX_LATCH;


-- Create single clock signal from differential clock signals arriving at FPGA.

makeClock : entity work.createClock
  port map (
     SYSCLK_P   => SYSCLK_P, 
     SYSCLK_N   => SYSCLK_N, 
     CLK        => CLK,
     CLKslow    => CLKslow
  );


-- Latch some signals,

latches : process (CLK, CLKslow)
begin
  if rising_edge(CLK) then
    -- Hit FIFO read latency is 1, so start track reco 1 clk after 
    -- FIFO starts outputting hits.
    START_TRK <= START_HIT;

    -- Note if track reco is finished.
    -- DONE says step finished, but it is undefined until READY signal asserted.
    if (READY_TRK = '1' and DONE_TRK = '1') then
      FINISHED_TRK  <= '1';
    end if;
  end if;

  if rising_edge(CLKslow) then
    -- Notes if UART tranmission is finished.
    if (READY_UART = '1' and DONE_UART = '1') then
      FINISHED_UART <= '1';
    end if;

    -- Records if there was ever any activity on the USB/UART line.
    if UART_TX = '0' then
      UART_TX_LATCH <= '1';
    end if; 
  end if;

end process;


-- Pass signals across boundary from CLK to CLKslow domain.

crossing : process (CLK, CLKslow)
begin

  if rising_edge(CLK) then
    if FINISHED_TRK = '1' then
      FINISHED_TRK_LATCH <= '1';
    end if;     
  end if;

  if rising_edge(CLKslow) then
    -- The trick is to register each of them twice in succession.
    RESET_REG1 <= RESET;
    RESET_REG2 <= RESET_REG1;
    FINISHED_TRK_REG1 <= FINISHED_TRK_LATCH;    
    FINISHED_TRK_REG2 <= FINISHED_TRK_REG1;    
    NTRACKS_REG1 <= NTRACKS;
    NTRACKS_REG2 <= NTRACKS_REG1;
  end if;

end process;


-- Initialize control signals (reset & start)

init : process(CLK)
  constant maxCnt : INTEGER := 127;
  variable count : INTEGER RANGE 0 to maxCnt := 0;
begin
  if rising_edge(CLK) then
    IF (USE_BUTTON) then
      START_HIT <= START_BTN;
    else
      if count < maxCnt then
        count := count + 1;
        -- Unset "reset" signal a few clock cycles after the FPGA is programmed.
        if count = 31 then
          RESET <= '0';
        elsif count = maxCnt then
          START_HIT <= '1';
        end if;  
      end if; 
    end if;
  end if;
end process;


-- Detect a press of central button on board.

button : entity work.button
  generic map (
    USE_BUTTON => USE_BUTTON
  )
  port map (
    CLK => CLK,
    BTN => BTN,
    PRESSED => START_BTN
  );


-- FIFO memory containing input hits (ready from data.txt).

readHits : entity work.readHitsIP
  port map (
     AP_CLK => CLK,
     AP_RST => RESET_HIT,
     AP_START => START_HIT,
     AP_DONE => READY_HIT,
     NEXTHIT_V => HIT,
     LAST_V(0) => LAST_HIT --! Indicates if this is the last hit available
);


-- BRAM memory for storing reconstructed tracks.

memTrks : entity work.memory
  generic map (
     WIDTH_BITS => WIDTH_BITS,
     DEPTH_BITS => DEPTH_BITS
  )
  port map (
     READ_CLK => CLKslow,
     READ_ENABLE => READ_ENABLE,
     READ_ADDR => READ_ADDR,
     READ_DATA => READ_DATA,
     WRITE_CLK => CLK,
     WRITE_ENABLE => WRITE_ENABLE,
     WRITE_ADDR => WRITE_ADDR,
     WRITE_DATA => WRITE_DATA
  );


-- Reconstruct tracks

trkReco : entity work.trkRecoIP
  port map (
     AP_CLK => CLK,
     AP_RST => RESET,
     AP_START => START_TRK,
     AP_DONE => READY_TRK,
     HIT_V => HIT,
     LAST_HIT_V(0) => LAST_HIT,
     DONE_HITS_READ_V(0) => DONE_TRK_READ_HITS, -- For debug only
     DONE_TRK_RECO_V(0) => DONE_TRK,
     NTRACKS_V => NTRACKS,
     TRACKS_V_ADDRESS0 => WRITE_ADDR,
     TRACKS_V_WE0 => WRITE_ENABLE,
     TRACKS_V_D0 => WRITE_DATA
  );


-- Display number of reconstrucetd tracks on LCD screen.

lcd: entity work.lcd16x2_ctrl_int 
  port map (
    CLOCK  => CLKslow,
    LCD_E  => LCD_E_LS,
    LCD_RS => LCD_RS_LS,
    LCD_RW => LCD_RW_LS,
    LCD_DB4 => LCD_DB4_LS,
    LCD_DB5 => LCD_DB5_LS,
    LCD_DB6 => LCD_DB6_LS,
    LCD_DB7 => LCD_DB7_LS,
    NumTracks => NTRACKS_REG2
  );

-- Transmit data of reconstructed tracks over USB/UART link to Linux.

uart : entity work.sendDataIP
  port map (
     AP_CLK => CLKslow,
     AP_RST => RESET_REG2,
     AP_START => FINISHED_TRK_REG2,   -- Start this when tracking is finished
     AP_DONE => READY_UART,     
     DONE_V(0) => DONE_UART,
     nDATA_V => NTRACKS_REG2,
     DATA_V_ADDRESS0 => READ_ADDR,
     DATA_V_CE0 => READ_ENABLE,
     DATA_V_Q0 => READ_DATA,
     UART_TXD_V(0) => UART_TX
  );

end Behavioral;
