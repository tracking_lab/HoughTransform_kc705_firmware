library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity countTracks is
    Generic ( WIDTH_BITS : natural;
    	      DEPTH_BITS : natural);
    Port ( START        : in  std_logic;
           READ_CLK     : in  std_logic;
	   READ_DATA    : in  std_logic_vector(WIDTH_BITS - 1 downto 0);
           DONE         : out std_logic;
	   READ_ENABLE  : out std_logic;
	   READ_ADDR    : out std_logic_vector(DEPTH_BITS - 1 downto 0);
           NTRACKS      : out std_logic_vector(DEPTH_BITS - 1 downto 0));
end countTracks;

architecture Behavioral of countTracks is
             
  signal NTRK : unsigned(DEPTH_BITS - 1 downto 0) := (others => '0');
  signal START_REG : std_logic := '0';
  signal DONE_TMP : std_logic := '0';
  signal READ_ENB : std_logic := '0';

  constant DEPTH : natural := DEPTH_BITS**2;
  type memType is array(0 to DEPTH_BITS-1) of std_logic_vector(WIDTH_BITS-1 downto 0);
  signal RAM : memType := (others => (others => '0'));
  attribute ram_style : string;
  attribute ram_style of RAM : signal is "block";
  
begin

readMem : process (READ_CLK)
  constant nullEntry : std_logic_vector(WIDTH_BITS - 1 downto 0) := (others => '1');
begin

  READ_ADDR <= std_logic_vector(NTRK);
  NTRACKS <= std_logic_vector(NTRK); -- Identical to READ_ADDR, but with meaningful name
  DONE <= DONE_TMP;

  if rising_edge(READ_CLK) then
    START_REG <= START; -- Delay start as takes 1 clk to read out memory.
    if START = '1' then
      READ_ENB <= NOT DONE_TMP;
    end if;
    if START_REG = '1' and NOT DONE_TMP = '1' then
       if (READ_DATA = nullEntry) then
         DONE_TMP <= '1'; -- Last read track was null, so we've finished
       elsif (NTRK = DEPTH) then
         DONE_TMP <= '1'; -- No more tracks would fit in RAM, so we've finished.
       else
         NTRK <= NTRK + 1; -- Read next track from memory.
       end if;        
    end if;
  end if;
end process;  

end Behavioral;
