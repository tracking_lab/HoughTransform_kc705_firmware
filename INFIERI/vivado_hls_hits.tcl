#---------------------------------------------------------------
# Compile HLS IP core for reading stubs file .txt file
# & streaming them out.
#---------------------------------------------------------------

delete_project WorkHLS_hits
open_project WorkHLS_hits

set CFLAGS {-std=c++11}

set_top readHits
add_files     src_hits/readHits.h     -cflags "$CFLAGS"
add_files     src_hits/readHits.cc    -cflags "$CFLAGS"
add_files -tb src_hits/tb.cc          -cflags "$CFLAGS"

set solution solution1
delete_solution $solution
open_solution $solution

# KC705 FPGA
set_part {xc7k325tffg900-2} -tool vivado

create_clock -period 100MHz -name default

# Allow HLS to use longer (easier to understand) variable names in reports.
config_compile -name_max_length 100
# Prevent HLS relaxing pipeline interval to meet timing
config_schedule -relax_ii_for_timing=0

csim_design -clean -compiler gcc -mflags "-j8"
csynth_design
cosim_design -trace_level all -rtl vhdl -O
export_design -rtl vhdl -format ip_catalog

exit
