#ifndef __sendData__
#define __sendData__

#include "ap_int.h"

// Parameters defining size of BRAM memory for storing tracks
#include "../src_common/trkMemorySpec.h"

// FPGA slow clock frequency.
static const float freq_FPGA_ = 100000000 / 16;
// Clock frequency of baud rate used for UART/USB transmission.
static const float freq_baud_rate_ = 115200;

// Logic state indicating null data on UART/USB line.
static const ap_uint<1> UART_NULL = 1;

// Transmit nData 32b words from data[] over UART/USB

void sendData(ap_uint<NBITS_DEPTH> nData, const ap_uint<NBITS_TRK> data[ramDepth_],
	      ap_uint<1>& done, ap_uint<1>& uart_txd);

// Transmit single ASCII symbol (4 bits) over UART/USB
void sendAscii(ap_uint<8> ascii, ap_uint<1> ascii_vld,
	       ap_uint<1>& ascii_accepted, ap_uint<1>& uart_txd);

// Gives one tick each period of the UART/USB baud rate.
ap_uint<1> clk_tick_baud();

#endif
