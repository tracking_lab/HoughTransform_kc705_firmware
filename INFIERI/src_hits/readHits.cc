#include "readHits.h"

void readHits(ap_uint<NBITS_HIT>& nextHit, ap_uint<1>& last) {

#pragma HLS INTERFACE ap_none port=last
#pragma HLS INTERFACE ap_none port=nextHit
#pragma HLS PIPELINE II=1
 
  static ap_uint<16> idx = 0;
  static ap_uint<3> hold = 1;

  if (idx < numHits) {
    nextHit = hits[idx];
    if (idx == numHits - 1) {
      last = 1;
    } else {
      last = 0;
    }
    if (hold < HOLD_LENGTH) {
      hold++;
    } else {
      hold = 1;
      idx++;
    }
  } else {
    nextHit = nullHit;
    last = 1;
  }
}
