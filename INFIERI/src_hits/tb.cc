#include <iostream>

#include "readHits.h"

int main() {

  ap_uint<1> last = 0;
  ap_uint<NBITS_HIT> hit = nullHit;

  //readHits(false, output);
  //%printf("Result before enable=%lld\n",(long long)output.read());

  unsigned int iErr = 0;

  for (unsigned int loop=0; loop<999; loop++) {

    readHits(hit, last);

    // HLS algo only changes output data for one call in HOLD_LENGTH.
    unsigned int idx = loop/HOLD_LENGTH;
    unsigned int hold = loop%HOLD_LENGTH;

    if (hold == 0) std::cout<<std::dec<<" Index = "<<idx<<" Last = "<<last<<" hit = "<<std::hex<<hit<<" vs expected "<<hits[idx];
    std::cout<<std::dec<<" Layer = "<<hit.range(MSB_LAY, LSB_LAY)<<std::endl;

    // Check output of HLS module consistent with expectation.
    if (hit != hits[idx]) iErr++;

    if (last == 1) break; // No more hits in FIFO.
  }
  return iErr;
}

