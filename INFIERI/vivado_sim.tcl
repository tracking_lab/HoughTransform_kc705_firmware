#------------------------------------------------------------------------------
# Create Vivado project containing top.vhd that instantiates all the 
# HLS IP cores, reads in the hits from data/, runs track reconstruction, 
# stores the found tracks in a BRAM memory,
# transmits contents of BRAM over USB/UART to Linux,
# and displays the number of tracks on the LCD display.
#
# Also run Vivado simulation using test-bench tb.vhd .
#------------------------------------------------------------------------------

set projName WorkVHDL

set_param general.maxThreads 8

create_project -force ${projName} ${projName}
set_property  target_language  VHDL  [current_project]
# KC705 FPGA
set_property  part  xc7k325tffg900-2  [current_project]

# Rebuild user HLS IP repos index before adding any source files.
set_property  ip_repo_paths  {"WorkHLS_uart" "WorkHLS_trk" "WorkHLS_hits"}  [get_filesets sources_1]

# Create .xci files for user HLS IP
create_ip -name readHits -module_name readHitsIP -vendor xilinx.com -library hls -version 1.0
create_ip -name trkReco  -module_name trkRecoIP  -vendor xilinx.com -library hls -version 1.0
create_ip -name sendData -module_name sendDataIP -vendor xilinx.com -library hls -version 1.0

# Add VHDL code & test-bench.
add_files  -fileset constrs_1  KC705.xdc
add_files  -fileset constrs_1  clocks.xdc
add_files  -fileset sources_1  hdl/top.vhd
add_files  -fileset sources_1  hdl/createClock.vhd
add_files  -fileset sources_1  hdl/button.vhd
add_files  -fileset sources_1  hdl/memory.vhd
add_files  -fileset sources_1  hdl/lcd16x2_ctrl_int.vhd
add_files  -fileset sources_1  hdl/lcd16x2_ctrl.vhd
add_files  -fileset sim_1      hdl/tb.vhd
# Provide name of top-level HDL (without .vhd extension).
set_property top -value top -objects [get_filesets sources_1]
set_property top -value tb -objects [get_filesets sim_1]
update_compile_order -fileset sources_1

# Simulation
set_property xsim.simulate.runtime -value "2ms" -objects [get_filesets sim_1]
launch_simulation
#open_wave_config {tb_waves.wcfg}
